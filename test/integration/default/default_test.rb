# encoding: utf-8

# InSpec tests for gitlab-prometheus-git-exporter package

control 'general-pkg-checks' do
  impact 1.0
  title 'General tests for gitlab-prometheus-git-exporter package'
  desc '
    This control ensures that:
      * gitlab-prometheus-git-exporter package version 0.0.1 is installed

  '
  describe package('gitlab-prometheus-git-exporter') do
    it { should be_installed }
    its ('version') { should eq '0.0.1' }
  end
end

control 'general-service-checks' do
  impact 1.0
  title 'General tests for prometheus-git-exporter service'
  desc '
    This control ensures that:
      * prometheus-git-exporter service is installed, enabled and running
      * prometheus-git-exporter is listening on 0.0.0.0:9666
  '
  describe service('prometheus-git-exporter') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe port('9666') do
    # no idea why it shortens the process name.
    # does inspec parse netstat output? i'll be damned if yes
    its('processes') { should eq ['prometheus-git-'] }
    # TODO: change to ipv4 when binary supports it
    its('protocols') { should eq ['tcp'] }
    its('addresses') { should eq ['::'] }
  end
end
